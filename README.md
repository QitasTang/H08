﻿# [Platform H08](https://github.com/OS-Q/H08)
[![Build Status](https://travis-ci.com/OS-Q/H08.svg?branch=master)](https://travis-ci.com/OS-Q/H08)
[![Build status](https://ci.appveyor.com/api/projects/status/8ro1qlqcv7ash1aq?svg=true)](https://ci.appveyor.com/project/Qitas/H08)

[![sites](http://182.61.61.133/link/resources/OSQ.png)](http://www.OS-Q.com)

### [Platform描述](https://github.com/OS-Q/H08/wiki) 

[Platform H08](https://github.com/OS-Q/H08) 是基于PlatformIO开发平台的基础框架，用于在[ESP8266/ESP8285](https://github.com/sochub/ESP8266) 硬件上开发应用

### [OS-Q = Open Solutions | Open Source |  Operating System ](http://www.OS-Q.com/H08)
